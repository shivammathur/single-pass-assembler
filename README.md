# Single Pass Assembler
Single Pass Assembler for 16 bit machine in C

* Assembly code is in code.txt file.
* ISA along with opcodes is in isa.txt file.
* The assembler is in assm.c file.
* The final output is stored in output.txt file.
* For help with opcodes refer to help.txt file.
