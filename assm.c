/*
The MIT License (MIT)
Copyright (c) <2015> <Shivam Mathur - shivam_jpr@hotmail.com>
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#include <stdio.h> /* for I/0 and file handling functions */
#include <ctype.h> /* for character manipulation functions */
#include <string.h> /* for string manipulation function */
#include <stdlib.h> /* For exit(), strtol() functions*/

#if _WIN32 || _WIN64
#include <windows.h>	/* for clear screen function */
#endif

int codesize=0, isasize=0,tempsize=0; //global variables to hold size of the code file and isa file

char* readFile(char * file) {
	FILE *fptr;
	
	if ((fptr=fopen(file,"r"))==NULL){
    	printf("Error! opening file \"%s\"\n",file);
	}
	fseek(fptr, 0, SEEK_END);
	tempsize = ftell(fptr);
	fseek(fptr, 0, SEEK_SET);
	char *tempstr = malloc (sizeof (char) * tempsize);
	fscanf(fptr,"%[^\0]",tempstr); //reading content before the null character
	fclose(fptr);
	if(strcmp(file,"code.txt")==0)
		codesize = tempsize;
	if(strcmp(file,"isa.txt")==0)
		isasize = tempsize;
	return tempstr;	
}

char * hex2bin(char hex) {    //function to return binary of the hexadecimal code
	switch(hex){
        case '0': return "0000"; break;
        case '1': return "0001"; break;
        case '2': return "0010"; break;
        case '3': return "0011"; break;
        case '4': return "0100"; break;
        case '5': return "0101"; break;
        case '6': return "0110"; break;
        case '7': return "0111"; break;
        case '8': return "1000"; break;
        case '9': return "1001"; break;
        case 'A': return "1010"; break;
        case 'B': return "1011"; break;
        case 'C': return "1100"; break;
        case 'D': return "1101"; break;
        case 'E': return "1110"; break;
        case 'F': return "1111"; break;
		case 'a': return "1010"; break;
        case 'b': return "1011"; break;
        case 'c': return "1100"; break;
        case 'd': return "1101"; break;
        case 'e': return "1110"; break;
        case 'f': return "1111"; break;
        default:  printf("\nError! Invalid memory address\n");
    }
}

char * filterCode() { //function to return , seprated string of instructions and operands
	int i,j;
	char * code;
	code = readFile("code.txt");
	char *temp1 = malloc (sizeof (char) * codesize);
	char *temp2 = malloc (sizeof (char) * codesize);
	memset(temp1,'\0',codesize);
	memset(temp2,'\0',codesize);	
	for(i=0,j=0;code[i]!='\0';i++,j++){
		if (code[i]=='\n' || code[i]==' ')
			temp1[j]=',';
		else
			temp1[j]=code[i];
	}
	strcpy(temp2,temp1);
	char label[1][10];
	char* token = strtok(temp1, ",");	//spliting the string to read each token
	int startPos = 0, flag = 0, endPos = 0, flag1 = 0, labelLength =0, codeLength = 0 ;
    while (token != NULL) {
    	int length = strlen(token);
    	if(*(token+length-1)!=':' && flag == 0)
    		startPos += (length+1);
    	else if (*(token+length-1)==':' && flag == 0){ //detecting first label
    		labelLength = length;
    		if(length>10){
    			printf("Error! Max length for labels is 10 characters.\n");
				free(temp1);
				free(temp2);
				return NULL;
			}
    		
    		strcpy(label[0],token);
    		flag++;
    	}
    	else if (*(token+length-1)==':' && flag > 0) //detecting multiple labels
    		flag++;
    	if(strcmp(token,"JMP")!=0 && flag1 == 0) 
    		endPos += (length+1);
    	else if(strcmp(token,"JMP")==0 && flag1 == 0) //detecting first jump
    		flag1++;
    	else if(strcmp(token,"JMP")==0 && flag1 > 0) //detecting multiple jumps
    		flag1++;
		token = strtok(NULL, ",");
		
	}
	if (flag1>1 || flag > 1){
		printf("Error! Only 1 Jump and 1 Label is allowed.\n");
		free(temp1);
		free(temp2);
		return NULL;
	}
	
	strncpy(label[1],(temp2+endPos+4), labelLength-1);
	label[1][labelLength-1] = '\0';
	strcat(label[1],":");
	if (strcmp(label[0],label[1])!=0){
		printf("Error! Valid Label not detected\n");
		free(temp1);
		free(temp2);
		return NULL;
	}

	int endLength,jumpLength;
	char* startStr = malloc (sizeof (char) * startPos);
	memset(startStr,'\0',startPos);
	char* jumpStr = malloc (sizeof (char) * codesize);
	memset(jumpStr,'\0',codesize);
	char* endStr = malloc (sizeof (char) * endLength);
	memset(endStr,'\0',endLength);
	char* fullStr;

    if (startPos<endPos){
    	endLength = (strlen(temp2)-endPos-labelLength-5); //subtracting 5 to compensate for ,JMP,
		jumpLength = (endPos-startPos-labelLength+3); // adding 3 to get JMP copied
    	fullStr = malloc (sizeof (char) * (codesize+jumpLength));
		memset(fullStr,'\0',(codesize+jumpLength));
	    strncpy(startStr, temp2, startPos);
	    strncpy(endStr, (temp2+endPos+labelLength+4), endLength); //adding 4 to copy JMP
	    endStr[endLength] = '\0';
    	strncpy(jumpStr, (temp2+startPos+labelLength+1),jumpLength);
    	strcat(fullStr, startStr);
    	strcat(fullStr, jumpStr);
    	strncat(fullStr, jumpStr, (jumpLength-4)); //JMP should be copied only once
    	strcat(fullStr, endStr);
	}
	else{
		endLength = (strlen(temp2)-startPos-labelLength-2); //subtracting 2 to compensate for commas in ,label:,
	 	fullStr = malloc (sizeof (char) * (startPos+endLength+5));
		memset(fullStr,'\0',(startPos+endLength+5));
    	strncpy(startStr, temp2, endPos);
	    strncpy(endStr, (temp2+startPos+labelLength+1), endLength); //adding 4 to copy JMP
	    endStr[endLength] = '\0';
    	strcpy(jumpStr,"JMP,");
    	strcat(fullStr, startStr);
    	strcat(fullStr, jumpStr);
    	strcat(fullStr, endStr);
	}
	free(temp1);
	free(temp2);
	free(startStr);
	free(endStr);
	free(jumpStr);
	return fullStr;	
}

void decodeCode() {	
	FILE *fptr;
	if ((fptr=fopen("output.txt","w"))==NULL){
    	printf("Error! opening file");
		exit(1);        
	}	
	
	int i,j;	
	char* isa;
	char* temp2;
	temp2 = filterCode();
	if(temp2 == NULL)
		return;
	isa = readFile("isa.txt");
	char* token = strtok(temp2, ",");	//spliting the string to read each token
	int programCounter = 0; 
    while (token != NULL) {
    	int length = strlen(token);		
		char * result = strstr(isa,token);
		int position = result - isa;

		
		int	detectMemAdd = 0; // stores 1 if token is memory address
		for(i=0;i<length-1;i++){
			if(((isdigit(*(token+i)) || (*(token+i)>='a'&& *(token+i)<='f') || (*(token+i)>='A' && *(token+i)<='F'))))
				detectMemAdd = -1;
			else{
				detectMemAdd = 0;
				break;					
			}	
		}
		char * h;char * mem;
		long ret;
		
		int isAllDigit=0;
		
		for(i=0;i<length;i++){	//checking if token is all digits
			if(isdigit(token[i]))
				isAllDigit =1;
			else
				{
					isAllDigit =0;
					break;
				}
		}
		
		if(isAllDigit ==1){ //if token is all digits then printing the opcode
			ret = strtol(token,&h,10);
			if(ret>15){
				printf("\nError! Memory Overflow\n");
				printf("\nView Help in the Menu to correct your code in code.txt file\n");
			}
			else if (ret>0 && ret<10){
				char buff [2];
				int n=sprintf (buff, "%lu", ret);			
				printf(" %s",hex2bin(*buff));
				fprintf(fptr," %s",hex2bin(*buff));
			}
			else if (ret>9 && ret<16){
				char t = (char) ('a'+ret-10);
				printf(" %s",hex2bin(t));
				fprintf(fptr," %s",hex2bin(t));
			}
		}
		
		if(detectMemAdd == -1 && tolower(*(token+length-1)) == 'h'){ //checking if token is memory address
			detectMemAdd = 1;
			mem = (char*)malloc(4);
			memcpy(mem,token,3);
			mem[4] = '\0';
			ret = strtol(token,&h,10);
		}
		else
			detectMemAdd = 0;
			
		if( length >3 && isAllDigit !=1 && detectMemAdd == 1 && (isalpha(*mem) || isalpha(*(mem+1))) ) {  //checking if token is memory address and is more than 00FH.
			printf("\nError! Memory Overflow\n");
			printf("\nView Help in the Menu to correct your code in code.txt file\n");
			break;
		}
		
		if (length == 2 && detectMemAdd ==1 && isAllDigit !=1){ //if token is a memory address of length is 2 like AH,5H,etc
			printf(" %s",hex2bin(token[0]));
			fprintf(fptr," %s",hex2bin(token[0]));
		}

		//checking if token is memory address and is more than 00FH.			
		if (length >2  && isAllDigit !=1 && detectMemAdd == 1 && ((ret>9 && tolower(*h)=='h') || (ret ==0 && ((tolower(*h)>'f' && tolower(*h)<'a'))) || (ret > 0 && ((tolower(*h)<='f' && tolower(*h)>='a'))))){
			printf("\nError! Memory Overflow\n");
			printf("\nView Help in the Menu to correct your code in code.txt file\n");
			break;
		}
		//if token is a memory address of length more than 2 like 0AH, 05H, 00CH, etc
		else if (length >2  && isAllDigit !=1 && detectMemAdd == 1 && ((ret<=9 && tolower(*h)=='h') || (ret ==0 && ((tolower(*h)<='f' && tolower(*h)>='a'))))){
			if(ret!=0){
				char buffer [2];
				int n=sprintf (buffer, "%lu", ret);			
				printf(" %s",hex2bin(*buffer));
				fprintf(fptr," %s",hex2bin(*buffer));				
			}
			else{
				printf(" %s",hex2bin(*h));
				fprintf(fptr," %s",hex2bin(*h));
				}
		}

		if (result == '\0' && detectMemAdd !=1 && isAllDigit !=1) {	//checking if instruction is not found in the ISA	
			printf("\nError! \"%s\" not found in ISA | Exiting\n",token);
			printf("\nView Help in the Menu to correct your code in code.txt file\n");
			break;
		}
		int lineCounter=1;
		for(i=0; i<isasize;i++){	//getting the line number of each instruction
			char* to = (char*) malloc(length+1);
			memset(to,'\0',length+1);				
  			strncpy(to, isa+i, length);
  			if (strcmp(to,token)==0)  			
  				break;
  			else if (*(isa+i)=='\n')  	
  				lineCounter++;  			

  		}
  		
  		int numOp;
  		if (lineCounter<14)
  			numOp=3;
  		else if(lineCounter>13 && lineCounter<26)
  			numOp=2;
  		else if(lineCounter>25 && lineCounter<31)
		    numOp=1;
		else if (lineCounter>30 && lineCounter<41)
			numOp=0;
		else if (lineCounter>40)
			numOp=-1;
	
		if(numOp>=0 && programCounter >0  && isAllDigit !=1){ 
			putchar('\n');
			programCounter++;
			fputc('\n', fptr);
		} else if(numOp==-1){
			putchar(' ');
			fputc(' ', fptr);			
		}
		if(detectMemAdd !=1 && isAllDigit !=1){
		    for(i=position+length+1;*(isa+i)!='\n';i++){
				putchar(*(isa+i));
				fputc(*(isa+i), fptr);
			}
		}
		if(programCounter == 0)
			programCounter++;	
			
		if(strcmp(token,"HLT")==0){
			printf("\nHALT FOUND | EXITING\n");
			break;			
		}
	//	programCounter++;	

		token = strtok(NULL, ",");
	}
	fclose(fptr);
}

void clearsc(){
	#if _WIN32 || _WIN64
	system("cls");
	#else
    // Assume POSIX
    system ("clear");
	#endif
	printf("Welcome\n");
	printf("Assembler for the 16 bit ISA\n\n\n");

}

int menu(){
	clearsc();
	printf("Menu\n");
	printf("1)  Enter 1 to show the code\n");
	printf("2)  Enter 2 to show the ISA\n");
	printf("3)  Enter 3 to show the output\n");
	printf("4)  Enter 4 to show the help\n");
	printf("5)  Enter 5 to exit\n\n");
	printf("Enter your choice:\t");
	int num;
	fflush(stdout);fflush(stdin);
	scanf("%d",&num);
	printf("\n\n");
	return num;	
}

int main() //main function
{
	char ch='y';
	while(1){
		int num = menu();
		if(num==1){
				clearsc();
				printf("Given Code\n\n%s", readFile("code.txt"));
			}
		else if(num==2){
				clearsc();
				printf("Current ISA with opcodes\n\n%s", readFile("isa.txt"));
			}
		else if(num==3){
				clearsc();
				printf("Given Code\n\n%s\n", readFile("code.txt"));
				printf("Assembled Instructions for the given code are\n\n");
				decodeCode();
			}
		else if(num==4){
				clearsc();
				printf("Help for the Assembler of our ISA\n\n%s", readFile("help.txt"));
			}
		else if(num==5){
				exit(1);
			}
			
		printf("\n\nPress \"Y\" to go to the Menu again\n");
		printf("Press \"N\" to exit\n");
		printf("Enter your choice:\t");
		fflush(stdout);fflush(stdin);
		scanf("%c",&ch);
		if(tolower(ch)!='y'&& tolower(ch)=='n')
			return 0;
	}
	
	return 0;
}				

/*
The MIT License (MIT)
Copyright (c) <2015> <Shivam Mathur - shivam_jpr@hotmail.com>
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
